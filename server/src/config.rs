use {
    std::{
        fs::File,
        io::{BufReader, Read},
        path::Path,
    },
    toml,
};

pub const CONFIG_PATH: &str = "./config/config.toml";

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub host: Host,
    pub jira: Jira,
    pub ssl: Ssl,
    pub cookie: Cookie,
    pub jwt: Jwt,
    pub database: Database,
    pub server: Server,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Host {
    pub base_url: String,
    pub cache: bool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Jira {
    pub host: String,
    pub username: String,
    pub password: String,
    pub core_jql_conditions: String,
    pub cst_jql_conditions: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Ssl {
    pub cert: String,
    pub key: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Cookie {
    pub secret: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Jwt {
    pub secret: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Database {
    pub database_url: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Server {
    pub listen_socket: String,
}

impl Config {
    pub fn get_config(path: &str) -> Self {
        let mut config = String::new();

        if !Path::new(&path).exists() {
            error!("no config.toml found, exiting");
            ::std::process::exit(0);
        }
        let f = File::open(path).expect("Unable to open config.toml");
        let mut br = BufReader::new(f);
        br.read_to_string(&mut config)
            .expect("Unable to read config");
        let config: Config = match toml::from_str(&config) {
            Ok(config) => config,
            Err(e) => panic!("{}", e),
        };
        config
    }
    pub fn get_jira_api_uri(&self) -> String {
        format!("{}/rest/api/3", self.jira.host)
    }
}

impl Default for Config {
    fn default() -> Self {
        Config::get_config(CONFIG_PATH)
    }
}
