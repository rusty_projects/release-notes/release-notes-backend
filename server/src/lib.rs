#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

use {
    crate::{errors::CustomError, models::DbExecutor},
    actix::{Addr, SyncArbiter},
    actix_identity::{CookieIdentityPolicy, IdentityService},
    actix_web::{client::Client, middleware, App, HttpServer},
    chrono::Duration,
    config::Config,
    database::{diesel::result::Error, orm::ConnectionPool, redis::RedisConnectionPool},
    djangohashers::{make_password_with_algorithm, Algorithm::BCryptSHA256},
    num_cpus,
    openssl::ssl::{SslAcceptor, SslFiletype, SslMethod},
    openssl_probe::init_ssl_cert_env_vars,
};

pub mod api;
pub mod appconfig;
pub mod auth;
pub mod config;
pub mod errors;
pub mod models;

const DEFAULT_USER: &str = "administrator";
const DEFAULT_USER_PW: &str = "ChangeMeImmediately";
const ADMIN_GROUP: &str = "administrators";

lazy_static! {
    pub static ref THREADS: usize = num_cpus::get();
    pub static ref CONFIG: Config = Config::default();
    pub static ref DIESEL_PG: ConnectionPool = ConnectionPool::new(&CONFIG.database.database_url);
    // pub static ref REDIS_POOL: RedisConnectionPool = RedisConnectionPool::default();
}

pub type Result<T> = std::result::Result<T, CustomError>;

pub fn start_server() -> std::io::Result<()> {
    let sys = actix_rt::System::new("releasenoteswebsite");

    init_ssl_cert_env_vars();
    init_default_user();

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file(&CONFIG.ssl.key, SslFiletype::PEM)
        .unwrap();
    builder
        .set_certificate_chain_file(&CONFIG.ssl.cert)
        .unwrap();

    let address: Addr<DbExecutor> =
        SyncArbiter::start(*THREADS, move || DbExecutor(DIESEL_PG.clone()));

    HttpServer::new(move || {
        App::new()
            .data(DIESEL_PG.clone())
            .data(address.clone())
            .data(Client::default())
            // .data(REDIS_POOL.clone())
            .wrap(middleware::Logger::default())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&*CONFIG.cookie.secret.as_bytes())
                    .name("auth")
                    .path("/")
                    .max_age(Duration::weeks(1).num_seconds())
                    .secure(true),
            ))
            .configure(appconfig::config_app)
    })
    .bind_openssl(&CONFIG.server.listen_socket, builder)?
    .bind("127.0.0.1:8080")?
    .run();

    sys.run()
}

fn init_default_user() {
    let group = match DIESEL_PG.get_group(ADMIN_GROUP) {
        Ok(g) => g,
        Err(e) => match e {
            Error::NotFound => DIESEL_PG
                .create_group(ADMIN_GROUP)
                .expect("Failed creating the default admin group"),
            e => panic!("{}", e),
        },
    };
    let user = match DIESEL_PG.get_user(DEFAULT_USER) {
        Ok(u) => u,
        Err(e) => match e {
            Error::NotFound => DIESEL_PG
                .create_user(
                    DEFAULT_USER,
                    &make_password_with_algorithm(DEFAULT_USER_PW, BCryptSHA256),
                )
                .expect("Failed creating the default admin user"),
            e => panic!("{}", e),
        },
    };
    let groups = match DIESEL_PG.get_groups_for_user(&user) {
        Ok(g) => g,
        Err(e) => panic!("{}", e),
    };
    let result: Vec<_> = groups
        .into_iter()
        .filter(|g| g.groupname == ADMIN_GROUP)
        .collect();
    if result.is_empty() {
        let _usergroup = match DIESEL_PG.create_user_group_relation(&user, &group) {
            Ok(ug) => ug,
            Err(e) => panic!("{}", e),
        };
    }
}
