use {
    actix::{Actor, SyncContext},
    database::orm::ConnectionPool,
};

pub struct DbExecutor(pub ConnectionPool);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}
