use {
    crate::{
        api::{
            groups,
            interim::{projectgroups, usergroups},
            me, redis, releasenotes, users,
        },
        auth::routes,
    },
    actix_files::{Files, NamedFile},
    actix_web::{web, Result},
};

async fn index() -> Result<NamedFile> {
    Ok(NamedFile::open("static/index.html")?)
}

pub fn config_app(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api").service(
            web::scope("/1")
                .service(
                    web::resource("/auth")
                        .route(web::post().to(routes::login))
                        .route(web::delete().to(routes::logout))
                        .route(web::get().to(routes::get_me)),
                )
                .service(
                    web::scope("/releasenotes")
                        .service(
                            web::resource("/project")
                                .route(web::get().to(releasenotes::get_projects)),
                        )
                        .service(
                            web::scope("/{key}")
                                .service(
                                    web::resource("/versions")
                                        .route(web::get().to(releasenotes::get_versions_fut)),
                                )
                                .service(
                                    web::resource("/issues")
                                        .route(web::post().to(releasenotes::fut_post_issues)),
                                )
                                .service(web::resource("/search").route(
                                    web::post().to(releasenotes::fut_post_full_text_search),
                                )),
                        ),
                )
                .service(
                    web::scope("/cache").service(
                        web::resource("/releasenotes")
                            .route(web::delete().to(releasenotes::delete_releasenotes_cache)),
                    ),
                )
                .service(
                    web::scope("/administration")
                        .service(
                            web::scope("/user").service(
                                web::resource("").route(web::post().to(users::create_user)),
                            ),
                        )
                        .service(web::resource("/users").route(web::get().to(users::get_all_users)))
                        .service(
                            web::resource("/groups").route(web::get().to(groups::get_all_groups)),
                        )
                        .service(
                            web::scope("/group").service(
                                web::resource("").route(web::post().to(groups::create_group)),
                            ),
                        )
                        .service(
                            web::scope("/usergroup").service(
                                web::resource("")
                                    .route(web::post().to(usergroups::create_user_group_relation))
                                    .route(web::get().to(usergroups::get_groups_for_user)),
                            ),
                        )
                        .service(web::scope("/projectgroup").service(
                            web::resource("").route(
                                web::post().to(projectgroups::create_project_group_relation),
                            ),
                        )),
                )
                .service(
                    web::scope("/self")
                        .service(
                            web::resource("/password")
                                .route(web::post().to(users::change_own_password)),
                        )
                        .service(
                            web::resource("/groups")
                                .route(web::get().to(me::get_group_memberships)),
                        )
                        .service(
                            web::resource("/projects")
                                .route(web::get().to(me::get_visible_projects)),
                        ),
                )
                .service(
                    web::scope("/redis")
                        .service(web::resource("/ping").route(web::get().to(redis::redis_ping))),
                ),
        ),
    )
    .service(Files::new("/secure/attachment/", "./attachment"))
    .service(Files::new("/secure/thumbnail/", "./attachment"))
    .service(
        Files::new("/", "./static/")
            .index_file("static/index.html")
            .default_handler(web::resource("").to(index)),
    );
}
