use {
    super::API,
    crate::{Result, CONFIG},
    natord::compare,
    rayon::prelude::*,
    regex::{Regex, RegexSet},
    serde_json::{value::to_value, Value},
};

lazy_static! {
    static ref JIRA_BASE_URL: Regex = Regex::new(&CONFIG.jira.host).unwrap();
    static ref JIRA_ATTACHMENT: Regex =
        Regex::new(r#""(/secure/attachment/)(\d*)(/)(.*?\..*?)""#).unwrap();
    static ref THUMBNAIL_NAME: Regex = Regex::new(r#"\d*_(.*)"#).unwrap();
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Issues {
    pub expand: String,
    pub id: String,
    #[serde(rename = "self")]
    pub _self: String,
    pub key: String,
    #[serde(rename = "renderedFields")]
    pub rendered_fields: Option<RenderedFields>,
    pub fields: Fields,
}

impl Issues {
    pub async fn regex_ticket_content_for_links(&self, html: &str) {
        for cap in JIRA_ATTACHMENT.captures_iter(&html) {
            let image_url = format!("{}{}{}{}", &cap[1], &cap[2], &cap[3], &cap[4]);
            let caps = THUMBNAIL_NAME.captures(&cap[4]).unwrap();
            let t_name = caps.get(1).unwrap().as_str();
            API.create_image(&image_url, &cap[2], &cap[4], &t_name)
                .await;
        }
    }
    pub async fn replace_and_download_attachments(&self) {
        if let Some(rf) = &self.rendered_fields {
            if let Some(d) = &rf.description {
                self.regex_ticket_content_for_links(&d).await;
            }
            if let Some(i) = &rf.customfield_10082 {
                self.regex_ticket_content_for_links(&i).await;
            }
        }
    }
    pub fn replace_description(&mut self) {
        if let Some(rf) = self.rendered_fields.as_mut() {
            rf.replace_description();
            rf.replace_implementation();
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct RenderedFields {
    pub summary: Option<String>,
    pub issuetype: Option<String>,
    pub customfield_10082: Option<String>,
    pub description: Option<String>,
    pub customfield_10099: Option<String>,
    pub customfield_10100: Option<String>,
}

impl RenderedFields {
    pub fn replace_description(&mut self) {
        if let Some(d) = self.description.as_mut() {
            self.description = Some(
                JIRA_BASE_URL
                    .replace_all(d, &*CONFIG.host.base_url)
                    .to_string(),
            );
        }
    }
    pub fn replace_implementation(&mut self) {
        if let Some(i) = self.customfield_10082.as_mut() {
            self.customfield_10082 = Some(
                JIRA_BASE_URL
                    .replace_all(i, &*CONFIG.host.base_url)
                    .to_string(),
            )
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct RootInterface {
    pub expand: Option<String>,
    #[serde(rename = "startAt")]
    pub start_at: i64,
    #[serde(rename = "maxResults")]
    pub max_results: i64,
    pub total: Option<i64>,
    pub issues: Vec<Issues>,
}

impl RootInterface {
    pub fn sort_dedup_issues(&mut self) {
        self.issues.sort_unstable_by(|a, b| compare(&b.key, &a.key));
        self.issues
            .dedup_by(|a, b| a.id.eq_ignore_ascii_case(&b.id));
    }
    pub fn sort_dedup_filter_issues(&mut self) {
        self.sort_dedup_issues();
        self.issues.retain(|i| {
            let col: Vec<_> = i
                .fields
                .fix_versions
                .par_iter()
                .filter(|r| if r.released { !r.archived } else { r.released })
                .collect();
            !col.is_empty()
        });
    }
    pub fn prepare_for_cache(&self) -> Result<Value> {
        let v = to_value(self.clone())?;
        Ok(v)
    }
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Attrs {
    pub layout: String,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Content {
    #[serde(rename = "type")]
    pub _type: String,
    pub content: Option<Vec<Content1>>,
    pub attrs: Option<Attrs>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Content1 {
    #[serde(rename = "type")]
    pub _type: String,
    pub text: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Content2 {
    #[serde(rename = "type")]
    pub _type: String,
    pub content: Option<Vec<Content1>>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Description {
    pub version: i64,
    #[serde(rename = "type")]
    pub _type: String,
    pub content: Option<Vec<Content2>>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Fields {
    pub customfield_10082: Option<Description>,
    pub customfield_10100: Option<Description>,
    pub issuetype: Issuetype1,
    pub description: Option<Description>,
    pub customfield_10099: Option<Description>,
    pub summary: String,
    #[serde(rename = "fixVersions")]
    pub fix_versions: Vec<FixVersions>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct FixVersions {
    #[serde(rename = "self")]
    pub _self: String,
    pub id: String,
    pub description: Option<String>,
    pub name: String,
    pub archived: bool,
    pub released: bool,
    #[serde(rename = "releaseDate")]
    pub release_date: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd, Debug)]
pub struct Issuetype1 {
    #[serde(rename = "self")]
    pub _self: String,
    pub id: String,
    pub description: Option<String>,
    #[serde(rename = "iconUrl")]
    pub icon_url: String,
    pub name: String,
    pub subtask: bool,
}
