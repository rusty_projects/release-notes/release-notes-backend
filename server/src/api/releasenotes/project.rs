#[derive(Serialize, Deserialize)]
pub struct ProjectCategory {
    #[serde(rename = "self")]
    pub _self: String,
    pub id: String,
    pub name: String,
    pub description: String,
}

#[derive(Serialize, Deserialize)]
pub struct RootInterface {
    pub expand: String,
    #[serde(rename = "self")]
    pub _self: String,
    pub id: String,
    pub key: String,
    pub name: String,
    #[serde(rename = "projectCategory")]
    pub project_category: Option<ProjectCategory>,
    #[serde(rename = "projectTypeKey")]
    pub project_type_key: String,
    pub simplified: bool,
    pub style: String,
    #[serde(rename = "isPrivate")]
    pub is_private: bool,
}
