use {
    super::{super::issues, models::Api},
    crate::{api::releasenotes::project, errors::CustomError, Result, CONFIG},
    actix_web::{
        client::Client,
        error,
        web::{BytesMut, Data},
        Error,
    },
    base64::encode,
    database::orm::ConnectionPool,
    futures::{future::join_all, prelude::*},
    futures_retry::{ErrorHandler, FutureRetry, RetryPolicy},
    rayon::prelude::*,
    std::time::Duration,
};

lazy_static! {
    static ref ENCODED_USER_PW: String = format!(
        "Basic {}",
        encode(&format!(
            "{}:{}",
            &CONFIG.jira.username, &CONFIG.jira.password
        ))
    );
}

const TRY_LATER: u64 = 5000;

struct IoHandler<D> {
    max_attempts: usize,
    current_attempt: usize,
    display_name: D,
}

impl<D> IoHandler<D> {
    fn new(max_attempts: usize, display_name: D) -> Self {
        IoHandler {
            max_attempts,
            current_attempt: 0,
            display_name,
        }
    }
}

impl<D> ErrorHandler<Error> for IoHandler<D>
where
    D: ::std::fmt::Display,
{
    type OutError = Error;

    fn handle(&mut self, e: Error) -> RetryPolicy<Error> {
        self.current_attempt += 1;
        if self.current_attempt > self.max_attempts {
            error!(
                "[{}] All attempts ({}) have been used",
                self.display_name, self.max_attempts
            );
            return RetryPolicy::ForwardError(e);
        }
        error!(
            "[{}] Attempt {}/{} has failed",
            self.display_name, self.current_attempt, self.max_attempts
        );
        match e {
            _ => RetryPolicy::WaitRetry(Duration::from_millis(TRY_LATER)),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JqlPost {
    pub jql: String,
    #[serde(rename = "startAt")]
    pub start_at: i64,
    #[serde(rename = "maxResults")]
    pub max_results: u32,
    pub fields: [String; 7],
    pub expand: [String; 1],
    #[serde(rename = "fieldsByKeys")]
    pub fields_by_keys: bool,
}

impl JqlPost {
    pub fn new(
        jql: &str,
        start_at: i64,
        max_results: u32,
        fields: [String; 7],
        expand: [String; 1],
        fields_by_keys: bool,
    ) -> Self {
        JqlPost {
            jql: jql.to_string(),
            start_at,
            max_results,
            fields,
            expand,
            fields_by_keys,
        }
    }
    pub fn get_fields() -> [String; 7] {
        let fields: [String; 7] = [
            String::from("description"),
            String::from("summary"),
            String::from("customfield_10082"),
            String::from("customfield_10100"),
            String::from("customfield_10099"),
            String::from("issuetype"),
            String::from("fixVersions"),
        ];
        fields
    }
    pub fn get_expand() -> [String; 1] {
        let expand: [String; 1] = [String::from("renderedFields")];
        expand
    }
    pub fn get_post_body(jql: &str, start_at: i64) -> Self {
        JqlPost {
            jql: jql.to_string(),
            start_at,
            ..Default::default()
        }
    }
}

impl Default for JqlPost {
    // Default is not actually useable
    fn default() -> Self {
        JqlPost {
            jql: String::from(""),
            start_at: 0,
            max_results: 100,
            fields: JqlPost::get_fields(),
            expand: JqlPost::get_expand(),
            fields_by_keys: true,
        }
    }
}

impl Api {
    pub async fn fut_post_jql(
        self,
        jql_post: JqlPost,
        client: Client,
    ) -> std::result::Result<issues::RootInterface, Error> {
        let mut res = client
            .post(&format!("{}/search", self.base_url))
            .header("Authorization", ENCODED_USER_PW.to_owned())
            .send_json(&jql_post.clone())
            .await
            .map_err(Error::from)?;
        match res.status().as_u16() {
            200 => {
                let mut body = BytesMut::new();
                while let Some(chunk) = res.next().await {
                    body.extend_from_slice(&chunk?);
                }
                let body: issues::RootInterface =
                    serde_json::from_slice(&body).map_err(Error::from)?;
                Ok(body)
            }
            429 => Err(error::ErrorTooManyRequests("JIRA API limited")),
            n => panic!("JIRA responded with http status: {}", n),
        }
    }
    pub async fn fut_post_remaining_jql(
        &self,
        mut res: issues::RootInterface,
        jql: String,
        max_pages: i64,
        client: &Client,
    ) -> std::result::Result<issues::RootInterface, Error> {
        let mut vjql = vec![];
        for p in 1..=max_pages {
            vjql.push(JqlPost::get_post_body(&jql, p))
        }
        let mut v_futures = vec![];

        for j in vjql.into_iter() {
            let action = FutureRetry::new(
                {
                    let api = self.to_owned();
                    move || api.to_owned().fut_post_jql(j.clone(), client.clone())
                },
                IoHandler::new(20, "Running a client"),
            )
            .map_err(|e| error!("Failed getting Jira Response: {}", e))
            .map(|res| res);
            v_futures.push(action)
        }

        let f = join_all(v_futures).await;
        let results = f
            .into_par_iter()
            .collect::<std::result::Result<Vec<issues::RootInterface>, ()>>();
        if let Ok(result) = results {
            for mut v in result.into_iter() {
                res.issues.append(&mut v.issues)
            }
        } else {
            return Err(CustomError::InternalServerError.into());
        }
        Ok(res)
    }
    pub async fn fut_get_projects(
        &self,
        client: &Client,
        pool: Data<ConnectionPool>,
    ) -> std::result::Result<Vec<project::RootInterface>, Error> {
        let bytes = self.fut_get_response(client, "/project").await?;
        let projects = serde_json::from_slice::<Vec<project::RootInterface>>(&bytes)?;
        let insert_projects: Vec<_> = projects
            .par_iter()
            .map(|project| (&project.key, &project.name))
            .collect();
        if let Err(e) = pool.create_projects(insert_projects) {
            return Err(CustomError::from(e).into());
        }
        Ok(projects)
    }
    pub async fn fut_get_response(
        &self,
        client: &Client,
        route: &str,
    ) -> std::result::Result<BytesMut, Error> {
        let mut res = client
            .get(&format!("{}{}", self.base_url, route))
            .header("Authorization", ENCODED_USER_PW.to_owned())
            .send()
            .await
            .map_err(Error::from)?;
        let mut body = BytesMut::new();
        while let Some(chunk) = res.next().await {
            body.extend_from_slice(&chunk?);
        }
        Ok(body)
    }
    // does not follow redirects
    pub async fn get_basic_jira_url(
        &self,
        client: &Client,
        path: &str,
    ) -> std::result::Result<BytesMut, Error> {
        info!("{}{}", CONFIG.jira.host, path);
        let mut res = client
            .get(&format!("{}{}", CONFIG.jira.host, path))
            .header("Authorization", ENCODED_USER_PW.to_owned())
            .send()
            .await
            .map_err(Error::from)?;
        info!("{}", res.status().as_u16());
        let mut body = BytesMut::new();
        while let Some(chunk) = res.next().await {
            body.extend_from_slice(&chunk?);
        }
        Ok(body)
    }
    pub fn create_dir(&self, path: &str) -> std::io::Result<()> {
        if !std::path::Path::new(path).is_dir() {
            std::fs::create_dir(path)?
        }
        Ok(())
    }
    pub fn check_file(&self, path: &std::path::Path) -> bool {
        std::path::Path::new(path).exists()
    }
    pub async fn create_image(
        &self,
        route: &str,
        folder: &str,
        image_name: &str,
        thumbnail_name: &str,
    ) {
        let path = format!("./attachment/{}", folder);
        if self.check_file(std::path::Path::new(&format!("{}{}", folder, image_name))) {
            return;
        }
        if let Err(e) = self.create_dir(&path) {
            error!("{}", e);
        }
        let oof = format!("{}{}", CONFIG.jira.host, route);
        let client = reqwest::Client::new();
        let mut res = client
            .get(&oof)
            .basic_auth(&CONFIG.jira.username, Some(&CONFIG.jira.password))
            .send()
            .await
            .unwrap();
        let mut body = BytesMut::new();
        while let Some(chunk) = res.chunk().await.unwrap() {
            body.extend_from_slice(&chunk);
        }
        let image_path = format!("{}/{}", path, image_name);
        let mut file = async_std::fs::File::create(&image_path).await.unwrap();
        if let Err(e) = file.write_all(&body).await {
            error!("{}", e);
        }
        let thumbnail_path = format!("{}/{}", path, thumbnail_name);
        if self.check_file(std::path::Path::new(&thumbnail_path)) {
            return;
        }
        self.create_thumbnail(&body, &thumbnail_path);
    }
    pub fn create_thumbnail(&self, b: &[u8], file_path: &str) {
        use image::load_from_memory;
        use image::GenericImageView;

        let something = load_from_memory(b);
        if let Ok(pic) = something {
            let dimensions = pic.dimensions();
            let thumb = pic.thumbnail(dimensions.0 / 2, dimensions.1 / 2);
            if let Err(e) = thumb.save(file_path) {
                error!("{}", e);
            }
        }
    }
}
