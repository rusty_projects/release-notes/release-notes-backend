use {
    super::{
        issues::{Issues, RootInterface},
        API,
    },
    crate::{
        api::releasenotes::jira::api::JqlPost, auth::models::LoggedUser, errors::CustomError,
        Result, CONFIG,
    },
    actix_web::{
        client::Client,
        web::{Data, Path},
        Error,
    },
    database::orm::{models::projects::Project as DbProject, ConnectionPool},
    rayon::prelude::*,
    regex::{Regex, RegexSet},
    serde_json::value::from_value,
};

lazy_static! {
    static ref REGEX_JIRA_JQL: RegexSet = RegexSet::new(&[
        r"(?i)(([^a-z]|^)(AND|OR|NOT|WHERE)(\s*))",
        r"([^A-Za-z0-9_äÄöÖüÜß\s]+)",
    ])
    .unwrap();
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct QueryRelease {
    pub name: String,
}

impl QueryRelease {
    fn return_jql(project: Path<Project>, result: Vec<Release>) -> String {
        format!(
            "project = {} AND fixVersion in (\"{}\") {} ORDER BY issuetype ASC, key ASC",
            project.key,
            result[0].name,
            {
                if project.key == "SC" || project.key == "CL" || project.key == "SCC" {
                    &CONFIG.jira.core_jql_conditions
                } else {
                    &CONFIG.jira.cst_jql_conditions
                }
            }
        )
    }
    pub async fn manipulate_issue(&self, v: Vec<Issues>) -> Vec<Issues> {
        let mut tmp_vec = Vec::new();
        for mut ticket in v {
            ticket.replace_description();
            ticket.replace_and_download_attachments().await;
            tmp_vec.push(ticket);
        }
        tmp_vec
    }
    pub async fn fut_post_release(
        self,
        client: Data<Client>,
        project: Path<Project>,
        pool: Data<ConnectionPool>,
    ) -> std::result::Result<Vec<Issues>, Error> {
        if let Ok(o) = pool.get_cache_releasenote(&self.name) {
            info!("{}", o.created_at);
            match from_value::<RootInterface>(o.jira_response) {
                Ok(res) => {
                    let nice_work = self.manipulate_issue(res.issues).await;
                    Ok(nice_work)
                }
                Err(e) => Err(Error::from(e)),
            }
        } else {
            info!("Not cached!");
            let filtered_versions = project.fut_get_versions_filtered(&client).await?;
            let result: Vec<Release> = filtered_versions
                .into_par_iter()
                .filter(|v| v.name == self.name)
                .collect();
            if !result.is_empty() {
                let jql = QueryRelease::return_jql(project, result);
                let jql_post = JqlPost::get_post_body(&jql, 0);
                let mut res = API
                    .to_owned()
                    .fut_post_jql(jql_post, Client::default())
                    .await?;
                if let Some(max_pages) = res.total {
                    let mut res = API
                        .fut_post_remaining_jql(res, jql.to_owned(), max_pages, &client)
                        .await?;
                    res.sort_dedup_issues();
                    if CONFIG.host.cache == true {
                        if let Err(e) = self.cache_query(pool, &res) {
                            error!("Failed saving release into database: {}", e);
                        }
                    }

                    Ok(self.manipulate_issue(res.issues).await)
                } else {
                    res.sort_dedup_issues();
                    if CONFIG.host.cache == true {
                        if let Err(e) = self.cache_query(pool, &res) {
                            error!("Failed saving release into database: {}", e);
                        }
                    }
                    Ok(self.manipulate_issue(res.issues).await)
                }
            } else {
                Err(Error::from(CustomError::JiraError(
                    "Release version does not exist".into(),
                )))
            }
        }
    }
    pub fn cache_query(&self, pool: Data<ConnectionPool>, jira_res: &RootInterface) -> Result<()> {
        let cache = jira_res.prepare_for_cache()?;
        pool.create_cache_releasenote(&self.name, cache)?;
        Ok(())
    }
    pub fn delete_cache_releasenotes(&self, pool: Data<ConnectionPool>) -> Result<()> {
        pool.delete_cache_releasenote(&self.name)?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Release {
    #[serde(rename = "self")]
    pub _self: String,
    pub id: String,
    pub name: String,
    pub archived: Option<bool>,
    pub released: bool,
    #[serde(rename = "projectId")]
    pub project_id: i64,
    pub description: Option<String>,
    #[serde(rename = "startDate")]
    pub start_date: Option<String>,
    #[serde(rename = "releaseDate")]
    pub release_date: Option<String>,
    #[serde(rename = "userStartDate")]
    pub user_start_date: Option<String>,
    #[serde(rename = "userReleaseDate")]
    pub user_release_date: Option<String>,
    pub overdue: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Project {
    pub key: String,
}

impl Project {
    pub async fn fut_get_versions(
        &self,
        client: &Client,
    ) -> std::result::Result<Vec<Release>, Error> {
        let bytes = API
            .fut_get_response(client, &format!("/project/{}/versions", self.key))
            .await?;
        serde_json::from_slice::<Vec<Release>>(&bytes).map_err(Error::from)
    }

    pub async fn fut_get_versions_filtered(
        &self,
        client: &Client,
    ) -> std::result::Result<Vec<Release>, Error> {
        let unfiltered_versions = self.fut_get_versions(client).await?;
        Ok(unfiltered_versions
            .into_par_iter()
            .filter(|r| {
                if r.released {
                    if let Some(archived) = r.archived {
                        !archived
                    } else {
                        true
                    }
                } else {
                    false
                }
            })
            .collect::<Vec<_>>())
    }
}

impl From<&DbProject> for Project {
    fn from(p: &DbProject) -> Self {
        Project { key: p.key.clone() }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FullTextSearch {
    pub search_term: String,
}

impl FullTextSearch {
    pub async fn manipulate_issue(&self, v: Vec<Issues>) -> Vec<Issues> {
        let mut tmp_vec = Vec::new();
        for mut ticket in v {
            ticket.replace_description();
            ticket.replace_and_download_attachments().await;
            tmp_vec.push(ticket);
        }
        tmp_vec
    }
    pub async fn fut_post_search_results(
        &self,
        client: Data<Client>,
        project: Path<Project>,
    ) -> std::result::Result<Vec<Issues>, Error> {
        let matches: Vec<_> = REGEX_JIRA_JQL
            .matches(&self.search_term)
            .into_iter()
            .collect();
        if matches.is_empty() {
            let jql = format!("project = {0} {1} AND (description ~ \"{2}\" OR summary ~ \"{2}\" OR Implementation ~ \"{2}\" OR Parameterization ~ \"{2}\" OR Influences ~ \"{2}\") ORDER BY issuetype ASC, key ASC", project.key, CONFIG.jira.core_jql_conditions, self.search_term);
            let jql_post = JqlPost::get_post_body(&jql, 0);
            let mut res = API
                .to_owned()
                .fut_post_jql(jql_post, Client::default())
                .await?;
            if let Some(max_pages) = res.total {
                let mut res = API
                    .fut_post_remaining_jql(res, jql.to_owned(), max_pages, &client)
                    .await?;
                res.sort_dedup_filter_issues();
                Ok(self.manipulate_issue(res.issues).await)
            } else {
                res.sort_dedup_filter_issues();
                Ok(self.manipulate_issue(res.issues).await)
            }
        } else {
            Err(Error::from(CustomError::SearchError(
                "Forbidden keyword(s) or character(s) used".into(),
            )))
        }
    }
}
