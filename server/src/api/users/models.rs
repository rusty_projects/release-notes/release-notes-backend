use {
    crate::errors::CustomError,
    actix_web::web::Data,
    chrono::NaiveDateTime,
    database::orm::{models::users::User as DbUser, ConnectionPool},
    djangohashers::{make_password_with_algorithm, Algorithm::BCryptSHA256},
};

#[derive(Deserialize, Serialize, Debug)]
pub struct User {
    pub username: String,
    pub password: String,
}

impl User {
    pub fn create_user(&self, pool: Data<ConnectionPool>) -> Result<ResponseUser, CustomError> {
        pool.create_user(
            &self.username,
            &make_password_with_algorithm(&self.password, BCryptSHA256),
        )
        .map(ResponseUser::from)
        .map_err(CustomError::from)
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ResponseUser {
    pub username: String,
    pub created_at: NaiveDateTime,
}

impl From<DbUser> for ResponseUser {
    fn from(user: DbUser) -> Self {
        ResponseUser {
            username: user.username,
            created_at: user.created_at,
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Password {
    pub password: String,
}
