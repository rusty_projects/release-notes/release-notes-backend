use actix_web::{error::Error, web, HttpResponse};
use database::redis::RedisConnectionPool;

pub async fn redis_ping(redis: web::Data<RedisConnectionPool>) -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().json(redis.ping()))
}
