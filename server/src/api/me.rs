use {
    crate::auth::models::LoggedUser,
    actix_web::{Error, HttpResponse},
};

pub async fn get_group_memberships(loggeduser: LoggedUser) -> Result<HttpResponse, Error> {
    loggeduser
        .get_group_memberships()
        .map(|gs| HttpResponse::Ok().json(gs))
        .map_err(Error::from)
}

pub async fn get_visible_projects(loggeduser: LoggedUser) -> Result<HttpResponse, Error> {
    loggeduser
        .get_visible_projects()
        .map(|ps| HttpResponse::Ok().json(ps))
        .map_err(Error::from)
}
