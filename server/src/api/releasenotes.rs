use {
    crate::{auth::models::LoggedUser, errors::CustomError, CONFIG},
    actix_web::{client::Client, web, Error, HttpResponse},
    database::orm::ConnectionPool,
    jira::models::Api,
    models::{FullTextSearch, Project, QueryRelease},
};

lazy_static! {
    pub static ref API: Api = Api {
        base_url: CONFIG.get_jira_api_uri()
    };
}

pub mod issues;
pub mod jira;
pub mod models;
pub mod project;

pub async fn delete_releasenotes_cache(
    query: Option<web::Query<QueryRelease>>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, CustomError> {
    loggeduser.is_admin()?;
    if let Some(q) = query {
        if let Err(e) = q.delete_cache_releasenotes(pool) {
            return Err(CustomError::DatabaseError(e.to_string()));
        }
    } else if let Err(e) = pool.delete_cache_releasenotes() {
        return Err(CustomError::DatabaseError(e.to_string()));
    }
    Ok(HttpResponse::Ok().finish())
}

pub async fn fut_post_issues(
    client: web::Data<Client>,
    post: web::Json<QueryRelease>,
    project: web::Path<Project>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.has_access_to_project(project.key.to_owned())?;
    let res = post.clone().fut_post_release(client, project, pool).await?;
    Ok(HttpResponse::Ok().json(res))
}

pub async fn fut_post_full_text_search(
    post: web::Json<FullTextSearch>,
    project: web::Path<Project>,
    client: web::Data<Client>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.has_access_to_project(project.key.to_owned())?;
    if project.key == "SC" {
        let res = post.fut_post_search_results(client, project).await?;
        Ok(HttpResponse::Ok().json(res))
    } else {
        Ok(HttpResponse::InternalServerError().json("Not ready"))
    }
}

pub async fn get_projects(
    pool: web::Data<ConnectionPool>,
    client: web::Data<Client>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    let res = API.fut_get_projects(&client, pool).await?;
    Ok(HttpResponse::Ok().json(res))
}

pub async fn get_versions_fut(
    project: web::Path<Project>,
    loggeduser: LoggedUser,
    client: web::Data<Client>,
) -> Result<HttpResponse, Error> {
    loggeduser.has_access_to_project(project.key.to_owned())?;
    let res = project.fut_get_versions_filtered(&client).await?;
    Ok(HttpResponse::Ok().json(res))
}
