use {
    crate::auth::models::LoggedUser,
    actix_web::{web, Error, HttpResponse},
    database::orm::ConnectionPool,
    models::Group,
};

pub mod models;

pub async fn create_group(
    group: web::Json<Group>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    group
        .create_group(pool)
        .map(|g| HttpResponse::Ok().json(g))
        .map_err(Error::from)
}

pub async fn get_all_groups(
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    Group::get_all_groups(pool)
        .map(|vg| HttpResponse::Ok().json(vg))
        .map_err(Error::from)
}
