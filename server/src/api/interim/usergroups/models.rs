#[derive(Deserialize, Serialize, Debug)]
pub struct UserGroup {
    pub username: String,
    pub groupname: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct User {
    pub username: String,
}
