use {
    crate::errors::CustomError,
    actix_web::web::Data,
    database::orm::{models::interim_projectgroup::ProjectGroup as DbProjectGroup, ConnectionPool},
};

#[derive(Deserialize, Serialize, Debug)]
pub struct ProjectGroup {
    pub project_key: String,
    pub groupname: String,
}

pub struct Project {
    pub project_key: String,
}

impl ProjectGroup {
    pub fn add_project_to_group(
        &self,
        pool: Data<ConnectionPool>,
    ) -> Result<DbProjectGroup, CustomError> {
        pool.create_project_group_relation_s(&self.project_key, &self.groupname)
            .map_err(CustomError::from)
    }
}
