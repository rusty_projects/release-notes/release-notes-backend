use {
    crate::auth::models::LoggedUser,
    actix_web::{web, Error, HttpResponse},
    database::orm::ConnectionPool,
    models::ProjectGroup,
};

pub mod models;

pub async fn create_project_group_relation(
    project_group: web::Json<ProjectGroup>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    project_group
        .add_project_to_group(pool)
        .map(|pg| HttpResponse::Ok().json(pg))
        .map_err(Error::from)
}
