use {
    crate::{auth::models::LoggedUser, errors::CustomError},
    actix_web::{web, Error, HttpResponse},
    database::orm::ConnectionPool,
    models::{User, UserGroup},
};

pub mod models;

fn add_user_to_group(
    user_group: UserGroup,
    pool: web::Data<ConnectionPool>,
) -> Result<database::orm::models::interim_usergroup::UserGroup, CustomError> {
    pool.add_user_to_group(&user_group.username, &user_group.groupname)
        .map_err(CustomError::from)
}

pub async fn create_user_group_relation(
    user_group: web::Json<UserGroup>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    add_user_to_group(user_group.0, pool)
        .map(|ug| HttpResponse::Ok().json(ug))
        .map_err(Error::from)
}

fn get_groups_for_user_db(
    username: &str,
    pool: web::Data<ConnectionPool>,
) -> Result<Vec<database::orm::models::groups::Group>, CustomError> {
    pool.get_groups_for_user_s(username)
        .map_err(CustomError::from)
}

pub async fn get_groups_for_user(
    user: web::Query<User>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    get_groups_for_user_db(&user.username, pool)
        .map(|g| HttpResponse::Ok().json(g))
        .map_err(Error::from)
}
