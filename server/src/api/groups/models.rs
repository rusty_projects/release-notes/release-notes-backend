use {
    crate::errors::CustomError,
    actix_web::web::Data,
    chrono::NaiveDateTime,
    database::orm::{models::groups::Group as DbGroup, ConnectionPool},
    rayon::prelude::*,
};

#[derive(Deserialize, Serialize, Debug)]
pub struct Group {
    pub groupname: String,
}

impl Group {
    pub fn create_group(&self, pool: Data<ConnectionPool>) -> Result<ResponseGroup, CustomError> {
        pool.create_group(&self.groupname)
            .map(ResponseGroup::from)
            .map_err(CustomError::from)
    }
    pub fn get_all_groups(pool: Data<ConnectionPool>) -> Result<Vec<ResponseGroup>, CustomError> {
        pool.get_all_groups()
            .map(|o| {
                o.into_par_iter()
                    .map(ResponseGroup::from)
                    .collect::<Vec<_>>()
            })
            .map_err(CustomError::from)
    }
}

impl From<DbGroup> for Group {
    fn from(g: DbGroup) -> Self {
        Group {
            groupname: g.groupname,
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ResponseGroup {
    pub groupname: String,
    pub created_at: NaiveDateTime,
}

impl From<DbGroup> for ResponseGroup {
    fn from(g: DbGroup) -> Self {
        ResponseGroup {
            groupname: g.groupname,
            created_at: g.created_at,
        }
    }
}
