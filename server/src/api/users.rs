use {
    crate::{auth::models::LoggedUser, errors::CustomError},
    actix_web::{web, Error, HttpResponse},
    database::orm::ConnectionPool,
    models::{Password, ResponseUser, User},
    rayon::prelude::*,
};

pub mod models;

pub async fn create_user(
    user: web::Json<User>,
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    user.create_user(pool)
        .map(|u| HttpResponse::Ok().json(u))
        .map_err(Error::from)
}

pub async fn get_all_users(
    pool: web::Data<ConnectionPool>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, Error> {
    loggeduser.is_admin()?;
    match pool.get_all_users() {
        Ok(us) => Ok(HttpResponse::Ok().json({
            us.into_par_iter()
                .map(ResponseUser::from)
                .collect::<Vec<ResponseUser>>()
        })),
        Err(e) => {
            error!("{}", e);
            Err(Error::from(CustomError::from(e)))
        }
    }
}

pub async fn change_own_password(
    password: web::Json<Password>,
    loggeduser: LoggedUser,
) -> Result<HttpResponse, CustomError> {
    loggeduser
        .change_password(&password.password)
        .map(|_| HttpResponse::Ok().finish())
}
