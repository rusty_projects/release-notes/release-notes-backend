use {
    crate::{
        auth::models::{Payload, SaneUser},
        errors::CustomError,
        CONFIG,
    },
    frank_jwt::{decode, encode, validate_signature, Algorithm, ValidationOptions},
};

const ALGO: Algorithm = Algorithm::HS512;

pub fn create_token(user: &SaneUser) -> Result<String, CustomError> {
    let p = Payload::create_payload(&user.username, user.admin);
    let payload = serde_json::to_value(p).unwrap();
    encode(json!({}), &CONFIG.jwt.secret, &payload, ALGO).map_err(|e| {
        error!("{}", e);
        CustomError::InternalServerError
    })
}

pub fn validate_token(jwt: &str) -> Result<bool, CustomError> {
    validate_signature(&jwt.to_string(), &CONFIG.jwt.secret, ALGO).map_err(|e| {
        error!("{}", e);
        CustomError::InternalServerError
    })
}

pub fn decode_token(jwt: &str) -> Result<SaneUser, CustomError> {
    let (_header, payload) = decode(
        &jwt.to_string(),
        &CONFIG.jwt.secret,
        ALGO,
        &ValidationOptions::default(),
    )
    .map_err(|e| {
        error!("{}", e);
        CustomError::InternalServerError
    })?;
    let user: SaneUser = serde_json::from_value(payload).map_err(|e| {
        error!("{}", e);
        CustomError::InternalServerError
    })?;
    Ok(user)
}
