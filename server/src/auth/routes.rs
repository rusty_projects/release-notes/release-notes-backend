use {
    crate::{
        auth::{
            models::{AuthData, LoggedUser},
            utils::create_token,
        },
        DbExecutor,
    },
    actix::Addr,
    actix_identity::Identity,
    actix_web::{web, Error, HttpResponse, ResponseError},
};

// pub async fn login(
//     pool: web::Data<ConnectionPool>,
//     auth_data: web::Json<AuthData>,
//     id: Identity,
// ) -> Result<HttpResponse, CustomError> {
//     match auth_data.check_login() {
//         Ok(u) => {
//             let jwt = create_token(&u)?;
//             id.remember(jwt);
//             Ok(HttpResponse::Ok().finish())
//         }
//         Err(e) => Err(CustomError::from(e)),
//     }
// }

pub async fn login(
    auth_data: web::Json<AuthData>,
    db: web::Data<Addr<DbExecutor>>,
    id: Identity,
) -> Result<HttpResponse, Error> {
    let res = db.send(auth_data.into_inner()).await?;
    match res {
        Ok(user) => {
            let jwt = create_token(&user)?;
            id.remember(jwt);
            Ok(HttpResponse::Ok().into())
        }
        Err(e) => Ok(e.error_response()),
    }
}

pub async fn get_me(logged_user: LoggedUser) -> HttpResponse {
    HttpResponse::Ok().json(logged_user)
}

pub fn logout(id: Identity) -> HttpResponse {
    id.forget();
    HttpResponse::Ok().finish()
}
