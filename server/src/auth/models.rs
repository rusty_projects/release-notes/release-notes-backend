use {
    crate::{
        api::groups::models::Group, auth::utils::decode_token, errors::CustomError, DbExecutor,
        ADMIN_GROUP, DIESEL_PG,
    },
    actix::{Handler, Message},
    actix_identity::Identity,
    actix_web::{dev::Payload as ActixPayload, Error, FromRequest, HttpRequest},
    chrono::{Duration, Local},
    database::orm::models::{
        groups::Group as DbGroup, projects::Project as DbProject, users::User,
    },
    djangohashers::check_password,
    djangohashers::{make_password_with_algorithm, Algorithm::BCryptSHA256},
    futures::future::Future,
    parking_lot::Mutex,
    rayon::prelude::*,
    std::pin::Pin,
};

#[derive(Debug, Deserialize)]
pub struct AuthData {
    pub username: String,
    pub password: String,
}

impl AuthData {
    pub fn check_login(&self) -> Result<SaneUser, CustomError> {
        match DIESEL_PG.get_user(&self.username) {
            Ok(u) => {
                if let Ok(matching) = check_password(&self.password, &u.hashed_password) {
                    if matching {
                        Ok(u.into())
                    } else {
                        Err(CustomError::Unauthorized)
                    }
                } else {
                    Err(CustomError::Unauthorized)
                }
            }
            Err(e) => {
                error!("{}", e);
                Err(CustomError::Unauthorized)
            }
        }
    }
}

impl Message for AuthData {
    type Result = Result<SaneUser, CustomError>;
}

impl Handler<AuthData> for DbExecutor {
    type Result = Result<SaneUser, CustomError>;
    fn handle(&mut self, msg: AuthData, _: &mut Self::Context) -> Self::Result {
        match &self.0.get_user(&msg.username) {
            Ok(user) => {
                if let Ok(matching) = check_password(&msg.password, &user.hashed_password) {
                    if matching {
                        Ok(user.into())
                    } else {
                        Err(CustomError::Unauthorized)
                    }
                } else {
                    Err(CustomError::Unauthorized)
                }
            }
            Err(e) => {
                error!("{}", e);
                Err(CustomError::Unauthorized)
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SaneUser {
    pub username: String,
    pub admin: bool,
}

impl From<User> for SaneUser {
    fn from(user: User) -> Self {
        let mut admin = false;
        if let Ok(groups) = DIESEL_PG.get_groups_for_user(&user) {
            let result: Vec<_> = groups
                .into_iter()
                .filter(|g| g.groupname == ADMIN_GROUP)
                .collect();
            if !result.is_empty() {
                admin = true;
            }
        }
        SaneUser {
            username: user.username,
            admin,
        }
    }
}

impl From<&User> for SaneUser {
    fn from(user: &User) -> Self {
        let mut admin = false;
        if let Ok(groups) = DIESEL_PG.get_groups_for_user(&user) {
            let result: Vec<_> = groups
                .into_iter()
                .filter(|g| g.groupname == ADMIN_GROUP)
                .collect();
            if !result.is_empty() {
                admin = true;
            }
        }
        SaneUser {
            username: user.username.clone(),
            admin,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Payload {
    pub sub: String,
    pub username: String,
    pub admin: bool,
    pub iat: i64,
    pub exp: i64,
}

impl Payload {
    pub fn create_payload(username: &str, admin: bool) -> Self {
        Payload {
            sub: "auth".into(),
            username: username.to_string(),
            admin,
            iat: Local::now().timestamp(),
            exp: (Local::now() + Duration::weeks(1)).timestamp(),
        }
    }
}

pub type LoggedUser = SaneUser;

impl FromRequest for LoggedUser {
    type Config = ();
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<LoggedUser, Error>>>>;

    fn from_request(req: &HttpRequest, pl: &mut ActixPayload) -> Self::Future {
        let fut = Identity::from_request(req, pl);

        Box::pin(async move {
            if let Some(identity) = fut.await?.identity() {
                let user = decode_token(&identity)?;
                return Ok(user as LoggedUser);
            };
            Err(CustomError::Unauthorized.into())
        })
    }
}

impl LoggedUser {
    pub fn is_admin(&self) -> Result<(), CustomError> {
        let groups = match DIESEL_PG.get_groups_for_user_s(&self.username) {
            Err(e) => return Err(CustomError::from(e)),
            Ok(g) => g,
        };
        let result: Vec<_> = groups
            .into_par_iter()
            .filter(|g| g.groupname == ADMIN_GROUP)
            .collect();
        if result.is_empty() {
            Err(CustomError::Unauthorized)
        } else {
            Ok(())
        }
    }
    pub fn change_password(&self, password: &str) -> Result<(), CustomError> {
        match DIESEL_PG.update_user_password_s(
            &self.username,
            &make_password_with_algorithm(password, BCryptSHA256),
        ) {
            Err(e) => Err(CustomError::from(e)),
            Ok(_) => Ok(()),
        }
    }
    pub fn get_group_memberships(&self) -> Result<Vec<Group>, CustomError> {
        match DIESEL_PG.get_groups_for_user_s(&self.username) {
            Err(e) => Err(CustomError::from(e)),
            Ok(groups) => Ok(groups.into_par_iter().map(Group::from).collect()),
        }
    }
    pub fn get_visible_projects(&self) -> Result<Vec<DbProject>, CustomError> {
        let memberships = self.get_group_memberships()?;
        let visible_projects: Result<Vec<_>, CustomError> = memberships
            .into_par_iter()
            .map(|gn| {
                DIESEL_PG
                    .get_projects_from_group_s(&gn.groupname)
                    .map_err(CustomError::from)
            })
            .collect();
        let vec_in_vec = visible_projects?;
        let tmp_vec = Mutex::new(Vec::new());
        let _: Vec<_> = vec_in_vec
            .into_par_iter()
            .map(|v| {
                let mut vp: Vec<_> = v.into_par_iter().map(|p| p).collect();
                let mut t_vec = tmp_vec.lock();
                t_vec.append(&mut vp);
            })
            .collect();
        let projects: Vec<DbProject> = tmp_vec.lock().clone();
        Ok(projects)
    }
    pub fn has_access_to_project(&self, key: String) -> Result<(), CustomError> {
        let projects = self.get_visible_projects()?;
        let result: Vec<_> = projects
            .into_par_iter()
            .filter(|vp| vp.key == key)
            .collect();
        if result.is_empty() {
            Err(CustomError::Unauthorized)
        } else {
            Ok(())
        }
    }
}
