use {
    actix_web::{error::ResponseError, HttpResponse},
    database::diesel::result::{DatabaseErrorKind, Error as DieselError},
    derive_more::Display,
    serde_json::Error as SerdeJsonError,
};

#[derive(Debug, Display, Serialize, Deserialize)]
pub enum CustomError {
    #[display(fmt = "Internal Server Error")]
    InternalServerError,

    #[display(fmt = "Something bad happened: {}", _0)]
    DatabaseError(String),

    #[display(fmt = "Nice try my dude")]
    Unauthorized,

    #[display(fmt = "Jira Error: {}", _0)]
    JiraError(String),

    #[display(fmt = "Error: {}", _0)]
    SearchError(String),
}

impl ResponseError for CustomError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            CustomError::InternalServerError => HttpResponse::InternalServerError().into(),
            CustomError::DatabaseError(ref message) => {
                HttpResponse::InternalServerError().json(message)
            }
            CustomError::Unauthorized => HttpResponse::Unauthorized().into(),
            CustomError::JiraError(ref message) => {
                HttpResponse::InternalServerError().json(message)
            }
            CustomError::SearchError(ref message) => HttpResponse::BadRequest().json(message),
        }
    }
}

impl From<DieselError> for CustomError {
    fn from(error: DieselError) -> Self {
        match error {
            DieselError::DatabaseError(kind, info) => {
                if let DatabaseErrorKind::UniqueViolation = kind {
                    let message = info.details().unwrap_or_else(|| info.message()).to_string();
                    return CustomError::DatabaseError(message);
                }
                CustomError::InternalServerError
            }
            _ => CustomError::InternalServerError,
        }
    }
}

impl From<SerdeJsonError> for CustomError {
    fn from(error: SerdeJsonError) -> Self {
        error!("serde_json failed: {:#?}", error);
        CustomError::InternalServerError
    }
}
