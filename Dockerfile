FROM ekidd/rust-musl-builder AS build
COPY --chown=rust:rust . ./
RUN cargo build --release
