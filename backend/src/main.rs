#[macro_use]
extern crate log;

use {
    log4rs,
    server::start_server,
    std::{fs, io::ErrorKind},
};

fn main() {
    if let Err(e) = create_log_folder() {
        match e.kind() {
            ErrorKind::AlreadyExists => (),
            e => panic!("{:?}", e),
        }
    }

    // the webserver will crash if it does not find the static folder
    if let Err(e) = create_static_folder() {
        match e.kind() {
            ErrorKind::AlreadyExists => (),
            e => panic!("{:?}", e),
        }
    }

    log4rs::init_file("config/log4rs.toml", Default::default()).unwrap();

    info!(r#"¯\_(ツ)_/¯"#);
    start_server().unwrap();
}

fn create_log_folder() -> std::io::Result<()> {
    fs::create_dir("./logs")
}

fn create_static_folder() -> std::io::Result<()> {
    fs::create_dir("./static")
}
