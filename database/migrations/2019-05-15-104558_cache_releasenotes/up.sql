-- Your SQL goes here
CREATE TABLE cache_releasenotes (
  name TEXT PRIMARY KEY,
  jira_response JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
