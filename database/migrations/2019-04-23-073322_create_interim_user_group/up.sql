-- Your SQL goes here
CREATE TABLE interim_usergroup (
  user_id UUID references users(id),
  group_id UUID references groups(id),
  PRIMARY KEY (user_id, group_id)
)
