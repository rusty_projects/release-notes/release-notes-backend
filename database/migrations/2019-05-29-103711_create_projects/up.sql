-- Your SQL goes here
CREATE TABLE projects (
    key TEXT PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    logo BYTEA
)