-- Your SQL goes here
CREATE TABLE interim_projectgroup (
    project_key TEXT references projects(key),
    group_id UUID references groups(id),
    PRIMARY KEY (project_key, group_id)
)
