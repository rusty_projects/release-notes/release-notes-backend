use {
    diesel::r2d2::{ConnectionManager, Pool, PooledConnection},
    diesel::{prelude::*, result::Error},
    std::env,
};

pub mod cache_releasenotes;
pub mod groups;
pub mod interim_projectgroup;
pub mod interim_usergroup;
pub mod models;
pub mod projects;
pub mod users;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone)]
pub struct ConnectionPool {
    pool: Pool<ConnectionManager<PgConnection>>,
}

embed_migrations!("./migrations");

impl Default for ConnectionPool {
    fn default() -> Self {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set!");
        ConnectionPool::new(&database_url)
    }
}

impl ConnectionPool {
    pub fn new(database_url: &str) -> Self {
        let manager = ConnectionManager::<PgConnection>::new(database_url);

        let pool = Pool::builder()
            .build(manager)
            .expect("Failed creating database pool");
        info!("Running pending migrations...");
        let conn = (&pool).get().unwrap();
        if let Err(e) = embedded_migrations::run_with_output(&conn, &mut std::io::stdout()) {
            eprintln!(
                "[DB:embedded_migrations] Error while running pending migrations: {}",
                e
            );
        };

        ConnectionPool { pool }
    }

    pub fn connection(&self) -> PooledConnection<ConnectionManager<PgConnection>> {
        self.pool.get().unwrap()
    }

    pub fn ping(&self) -> bool {
        let conn = self.connection();
        diesel::sql_query(r#"SELECT 1"#).execute(&conn).is_ok()
    }
}
