table! {
    cache_releasenotes (name) {
        name -> Text,
        jira_response -> Jsonb,
        created_at -> Timestamp,
    }
}

table! {
    groups (id) {
        id -> Uuid,
        groupname -> Text,
        created_at -> Timestamp,
    }
}

table! {
    interim_projectgroup (project_key, group_id) {
        project_key -> Text,
        group_id -> Uuid,
    }
}

table! {
    interim_usergroup (user_id, group_id) {
        user_id -> Uuid,
        group_id -> Uuid,
    }
}

table! {
    projects (key) {
        key -> Text,
        name -> Text,
        logo -> Nullable<Bytea>,
    }
}

table! {
    users (id) {
        id -> Uuid,
        username -> Text,
        hashed_password -> Text,
        created_at -> Timestamp,
    }
}

joinable!(interim_projectgroup -> groups (group_id));
joinable!(interim_projectgroup -> projects (project_key));
joinable!(interim_usergroup -> groups (group_id));
joinable!(interim_usergroup -> users (user_id));

allow_tables_to_appear_in_same_query!(
    cache_releasenotes,
    groups,
    interim_projectgroup,
    interim_usergroup,
    projects,
    users,
);
