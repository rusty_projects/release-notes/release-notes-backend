use {
    super::models::projects::{NewProject, Project},
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    rayon::prelude::*,
};

impl ConnectionPool {
    // tuple (key, name) of project
    pub fn create_projects(&self, v: Vec<(&String, &String)>) -> Result<Option<Vec<Project>>> {
        use crate::schema::projects::dsl::*;
        let conn = self.connection();

        let insert_projects: Vec<_> = v
            .par_iter()
            .map(|k| NewProject {
                key: k.0,
                name: k.1,
                logo: None,
            })
            .collect();

        diesel::insert_into(projects)
            .values(insert_projects)
            .on_conflict(key)
            .do_nothing()
            .get_results::<Project>(&conn)
            .optional()
    }

    pub fn get_project(&self, pk: &str) -> Result<Project> {
        use crate::schema::projects::dsl::*;
        let conn = self.connection();

        projects.filter(key.eq(pk)).first::<Project>(&conn)
    }
}
