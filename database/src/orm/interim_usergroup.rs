use {
    super::models::{
        groups::Group,
        interim_usergroup::{NewUserGroup, UserGroup},
        users::User,
    },
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    rayon::prelude::*,
    uuid::Uuid,
};

impl ConnectionPool {
    pub fn create_user_group_relation(&self, u: &User, g: &Group) -> Result<UserGroup> {
        use crate::schema::interim_usergroup::dsl::*;
        let conn = self.connection();

        let user_group = NewUserGroup {
            user_id: u.id,
            group_id: g.id,
        };

        diesel::insert_into(interim_usergroup)
            .values(user_group)
            .get_result::<UserGroup>(&conn)
    }

    pub fn add_user_to_group(&self, username: &str, groupname: &str) -> Result<UserGroup> {
        let user = self.get_user(username)?;
        let group = self.get_group(groupname)?;
        self.create_user_group_relation(&user, &group)
    }

    pub fn add_group_to_user(&self, groupname: &str, username: &str) -> Result<UserGroup> {
        self.add_user_to_group(username, groupname)
    }

    pub fn get_groups_for_user(&self, u: &User) -> Result<Vec<Group>> {
        use crate::schema::interim_usergroup::dsl::*;
        let conn = self.connection();

        let user_group_ids = UserGroup::belonging_to(u)
            .select(group_id)
            .load::<Uuid>(&conn)?;

        let iter_groups: Result<Vec<_>> = user_group_ids
            .par_iter()
            .map(|g_id| self.get_group_using_id(g_id))
            .collect();
        iter_groups
    }

    pub fn get_groups_for_user_s(&self, username: &str) -> Result<Vec<Group>> {
        let user = self.get_user(username)?;
        self.get_groups_for_user(&user)
    }

    pub fn get_users_in_group_s(&self, groupname: &str) -> Result<Vec<User>> {
        let group = self.get_group(groupname)?;
        self.get_users_in_group(&group)
    }

    pub fn get_users_in_group(&self, g: &Group) -> Result<Vec<User>> {
        use crate::schema::interim_usergroup::dsl::*;
        let conn = self.connection();

        let group_user_id = UserGroup::belonging_to(g)
            .select(user_id)
            .load::<Uuid>(&conn)?;

        group_user_id
            .par_iter()
            .map(|u_id| self.get_user_using_id(u_id))
            .collect()
    }

    pub fn delete_user_from_all_group_s(&self, un: &str) -> Result<usize> {
        let u = self.get_user(un)?;
        self.delete_user_from_all_groups(&u)
    }

    pub fn delete_user_from_all_groups(&self, u: &User) -> Result<usize> {
        use crate::schema::interim_usergroup::dsl::*;
        let conn = self.connection();

        diesel::delete(interim_usergroup.filter(user_id.eq(u.id))).execute(&conn)
    }

    pub fn delete_user_from_group_s(&self, un: &str, gn: &str) -> Result<usize> {
        let user = self.get_user(un)?;
        let group = self.get_group(gn)?;

        self.delete_user_from_group(&user, &group)
    }

    pub fn delete_user_from_group(&self, u: &User, g: &Group) -> Result<usize> {
        use crate::schema::interim_usergroup::dsl::*;
        let conn = self.connection();

        diesel::delete(interim_usergroup.filter(user_id.eq(u.id).and(group_id.eq(g.id))))
            .execute(&conn)
    }

    pub fn delete_group_from_user_s(&self, gn: &str, un: &str) -> Result<usize> {
        self.delete_user_from_group_s(un, gn)
    }

    pub fn delete_group_from_user(&self, g: &Group, u: &User) -> Result<usize> {
        self.delete_user_from_group(u, g)
    }

    pub fn delete_user_from_groups_s(&self, un: &str, gns: Vec<String>) -> Result<Vec<usize>> {
        let user = self.get_user(un)?;
        let result: Result<Vec<_>> = gns.par_iter().map(|gn| self.get_group(&gn)).collect();
        let groups = result?;

        self.delete_user_from_groups(&user, groups)
    }

    pub fn delete_user_from_groups(&self, u: &User, gs: Vec<Group>) -> Result<Vec<usize>> {
        let result: Result<Vec<_>> = gs
            .par_iter()
            .map(|g| self.delete_user_from_group(&u, &g))
            .collect();
        result
    }
}
