use crate::schema::*;

#[derive(Insertable, Debug)]
#[table_name = "projects"]
pub struct NewProject<'a> {
    pub key: &'a str,
    pub name: &'a str,
    pub logo: Option<&'a [u8]>,
}

#[derive(Identifiable, Serialize, Deserialize, Queryable, Clone, Debug)]
#[primary_key(key)]
pub struct Project {
    pub key: String,
    pub name: String,
    pub logo: Option<Vec<u8>>,
}
