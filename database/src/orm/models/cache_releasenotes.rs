use {crate::schema::*, chrono::NaiveDateTime, serde_json::Value};

#[derive(Insertable)]
#[table_name = "cache_releasenotes"]
pub struct NewCacheReleasenote<'a> {
    pub name: &'a str,
    pub jira_response: &'a Value,
}

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct CacheReleasenote {
    pub name: String,
    pub jira_response: Value,
    pub created_at: NaiveDateTime,
}
