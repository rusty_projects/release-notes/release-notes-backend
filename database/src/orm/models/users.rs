use {crate::schema::*, chrono::NaiveDateTime, uuid::Uuid};

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub id: Uuid,
    pub username: &'a str,
    pub hashed_password: &'a str,
}

#[derive(Identifiable, Serialize, Deserialize, Queryable, Debug)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub hashed_password: String,
    pub created_at: NaiveDateTime,
}
