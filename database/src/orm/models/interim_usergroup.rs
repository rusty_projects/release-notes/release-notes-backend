use {
    super::{groups::Group, users::User},
    crate::schema::*,
    uuid::Uuid,
};

#[derive(Insertable)]
#[table_name = "interim_usergroup"]
pub struct NewUserGroup {
    pub user_id: Uuid,
    pub group_id: Uuid,
}

#[derive(Identifiable, Queryable, Associations, Serialize, Deserialize, Debug)]
#[primary_key(user_id, group_id)]
#[belongs_to(User)]
#[belongs_to(Group)]
#[table_name = "interim_usergroup"]
pub struct UserGroup {
    pub user_id: Uuid,
    pub group_id: Uuid,
}
