use {
    super::{groups::Group, projects::Project},
    crate::schema::*,
    uuid::Uuid,
};

#[derive(Insertable)]
#[table_name = "interim_projectgroup"]
pub struct NewProjectGroup<'a> {
    pub project_key: &'a str,
    pub group_id: Uuid,
}

#[derive(Identifiable, Queryable, Associations, Serialize, Deserialize, Debug)]
#[primary_key(project_key, group_id)]
#[belongs_to(Project, foreign_key = "project_key")]
#[belongs_to(Group)]
#[table_name = "interim_projectgroup"]
pub struct ProjectGroup {
    pub project_key: String,
    pub group_id: Uuid,
}
