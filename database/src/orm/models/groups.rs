use {crate::schema::*, chrono::NaiveDateTime, uuid::Uuid};

#[derive(Insertable, Debug)]
#[table_name = "groups"]
pub struct NewGroup<'a> {
    pub id: Uuid,
    pub groupname: &'a str,
}

#[derive(Identifiable, Serialize, Deserialize, Queryable, Debug, Clone)]
pub struct Group {
    pub id: Uuid,
    pub groupname: String,
    pub created_at: NaiveDateTime,
}
