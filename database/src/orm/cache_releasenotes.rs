use {
    super::models::cache_releasenotes::{CacheReleasenote, NewCacheReleasenote},
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    serde_json::Value,
};

impl ConnectionPool {
    pub fn create_cache_releasenote(
        &self,
        n: &str,
        releasenotes: Value,
    ) -> Result<CacheReleasenote> {
        use crate::schema::cache_releasenotes::dsl::*;
        let conn = self.connection();

        let new_cache_releasenote = NewCacheReleasenote {
            name: &n,
            jira_response: &releasenotes,
        };

        diesel::insert_into(cache_releasenotes)
            .values(new_cache_releasenote)
            .get_result::<CacheReleasenote>(&conn)
    }

    pub fn get_cache_releasenote(&self, n: &str) -> Result<CacheReleasenote> {
        use crate::schema::cache_releasenotes::dsl::*;
        let conn = self.connection();

        cache_releasenotes
            .filter(name.eq(n))
            .first::<CacheReleasenote>(&conn)
    }

    pub fn delete_cache_releasenote(&self, n: &str) -> Result<usize> {
        use crate::schema::cache_releasenotes::dsl::*;
        let conn = self.connection();

        diesel::delete(cache_releasenotes.filter(name.eq(n))).execute(&conn)
    }

    pub fn delete_cache_releasenotes(&self) -> Result<usize> {
        use crate::schema::cache_releasenotes::dsl::*;
        let conn = self.connection();

        diesel::delete(cache_releasenotes).execute(&conn)
    }
}
