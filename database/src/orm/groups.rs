use {
    super::models::groups::{Group, NewGroup},
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    uuid::Uuid,
};

impl ConnectionPool {
    pub fn create_group(&self, gn: &str) -> Result<Group> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        let new_group = NewGroup {
            id: Uuid::new_v4(),
            groupname: gn,
        };

        diesel::insert_into(groups)
            .values(new_group)
            .get_result::<Group>(&conn)
    }

    pub fn get_group(&self, gn: &str) -> Result<Group> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        groups.filter(groupname.eq(gn)).first::<Group>(&conn)
    }

    pub fn get_all_groups(&self) -> Result<Vec<Group>> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        groups.load::<Group>(&conn)
    }

    pub fn delete_group_s(&self, gn: &str) -> Result<usize> {
        let group = self.get_group(gn)?;
        self.delete_group(&group)
    }

    pub fn delete_group(&self, g: &Group) -> Result<usize> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        diesel::delete(groups.filter(id.eq(g.id))).execute(&conn)
    }

    // can lead to unexpected behavior
    pub fn _delete_all_groups(&self) -> Result<usize> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        diesel::delete(groups).execute(&conn)
    }

    pub fn get_group_using_id(&self, g_id: &Uuid) -> Result<Group> {
        use crate::schema::groups::dsl::*;
        let conn = self.connection();

        groups.filter(id.eq(g_id)).first::<Group>(&conn)
    }
}
