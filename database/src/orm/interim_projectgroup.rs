use {
    super::models::{
        groups::Group,
        interim_projectgroup::{NewProjectGroup, ProjectGroup},
        projects::Project,
    },
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    rayon::prelude::*,
    uuid::Uuid,
};

impl ConnectionPool {
    pub fn create_project_group_relation(&self, p: &Project, g: &Group) -> Result<ProjectGroup> {
        use crate::schema::interim_projectgroup::dsl::*;
        let conn = self.connection();

        let project_group = NewProjectGroup {
            project_key: &p.key,
            group_id: g.id,
        };

        diesel::insert_into(interim_projectgroup)
            .values(project_group)
            .get_result::<ProjectGroup>(&conn)
    }

    pub fn create_project_group_relation_s(&self, pk: &str, gn: &str) -> Result<ProjectGroup> {
        let p = self.get_project(pk)?;
        let g = self.get_group(gn)?;
        self.create_project_group_relation(&p, &g)
    }

    pub fn get_groups_for_project(&self, p: &Project) -> Result<Vec<Group>> {
        use crate::schema::interim_projectgroup::dsl::*;
        let conn = self.connection();

        let project_group_ids = ProjectGroup::belonging_to(p)
            .select(group_id)
            .load::<Uuid>(&conn)?;

        let iter_groups: Result<Vec<_>> = project_group_ids
            .par_iter()
            .map(|g_id| self.get_group_using_id(g_id))
            .collect();
        iter_groups
    }

    pub fn get_groups_for_project_s(&self, pk: &str) -> Result<Vec<Group>> {
        let project = self.get_project(pk)?;
        self.get_groups_for_project(&project)
    }

    pub fn get_projects_from_group(&self, g: &Group) -> Result<Vec<Project>> {
        use crate::schema::interim_projectgroup::dsl::*;
        let conn = self.connection();

        let group_project_keys = ProjectGroup::belonging_to(g)
            .select(project_key)
            .load::<String>(&conn)?;

        let iter_projects: Result<Vec<_>> = group_project_keys
            .par_iter()
            .map(|p_key| self.get_project(p_key))
            .collect();
        iter_projects
    }

    pub fn get_projects_from_group_s(&self, gn: &str) -> Result<Vec<Project>> {
        let group = self.get_group(gn)?;
        self.get_projects_from_group(&group)
    }
}
