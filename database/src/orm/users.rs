use {
    super::models::users::{NewUser, User},
    crate::orm::{ConnectionPool, Result},
    diesel::prelude::*,
    uuid::Uuid,
};

impl ConnectionPool {
    pub fn create_user(&self, un: &str, hash: &str) -> Result<User> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        let new_user = NewUser {
            id: Uuid::new_v4(),
            username: un,
            hashed_password: hash,
        };

        diesel::insert_into(users)
            .values(new_user)
            .get_result::<User>(&conn)
    }

    pub fn get_user(&self, un: &str) -> Result<User> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        users.filter(username.eq(un)).first::<User>(&conn)
    }

    pub fn get_all_users(&self) -> Result<Vec<User>> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        users.load::<User>(&conn)
    }

    pub fn delete_user_s(&self, un: &str) -> Result<usize> {
        let u = self.get_user(un)?;
        self.delete_user(&u)
    }

    pub fn delete_user(&self, u: &User) -> Result<usize> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        diesel::delete(users.filter(id.eq(u.id))).execute(&conn)
    }

    // should not be used
    pub fn _delete_all_users(&self) -> Result<usize> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        diesel::delete(users).execute(&conn)
    }

    pub fn update_user_password_s(&self, un: &str, hash: &str) -> Result<usize> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        diesel::update(users.filter(username.eq(un)))
            .set(hashed_password.eq(hash))
            .execute(&conn)
    }

    pub fn update_user_password(&self, u: &User, hash: &str) -> Result<usize> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        diesel::update(users.filter(id.eq(u.id)))
            .set(hashed_password.eq(hash))
            .execute(&conn)
    }

    pub fn get_user_using_id(&self, u_id: &Uuid) -> Result<User> {
        use crate::schema::users::dsl::*;
        let conn = self.connection();

        users.filter(id.eq(u_id)).first::<User>(&conn)
    }
}
