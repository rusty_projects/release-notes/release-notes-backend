pub use r2d2_redis::r2d2;

use r2d2_redis::{redis, redis::Commands, RedisConnectionManager};

#[derive(Clone)]
pub struct RedisConnectionPool {
    pool: r2d2::Pool<r2d2_redis::RedisConnectionManager>,
}

impl Default for RedisConnectionPool {
    fn default() -> Self {
        let manager = RedisConnectionManager::new("redis://localhost").unwrap();
        let pool = r2d2::Pool::builder().build(manager).unwrap();
        RedisConnectionPool { pool }
    }
}

impl RedisConnectionPool {
    pub fn new(redis_uri: &str) -> RedisConnectionPool {
        let manager = RedisConnectionManager::new(redis_uri).unwrap();
        let pool = r2d2::Pool::builder().build(manager).unwrap();
        RedisConnectionPool { pool }
    }

    pub fn connection(&self) -> r2d2::PooledConnection<RedisConnectionManager> {
        self.pool.get().unwrap()
    }

    pub fn ping(&self) -> String {
        let mut conn = self.connection();
        let reply = redis::cmd("PING").query::<String>(&mut *conn).unwrap();
        assert_eq!("PONG", reply);
        reply
    }
}
